using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;

namespace ConsoleApp.Tasks
{
    /// <summary>
    /// Represents a backend task that processes multiple items concurrently.
    /// </summary>
    public class ThreadBackendTask : IBackendTask
    {
        protected readonly ILogger<ThreadBackendTask> Logger;
        protected const int ItemsCount = 100;
        
        /// <summary>
        /// Represents the configuration for a thread task item.
        /// </summary>
        protected record ThreadTaskItemConfig(int Number);
        
        /// <summary>
        /// Represents the result of a thread task item.
        /// </summary>
        protected record ThreadTaskItemResult(string Message, ThreadTaskItemConfig Config);
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadBackendTask"/> class.
        /// </summary>
        /// <param name="logger">The logger instance.</param>
        public ThreadBackendTask(ILogger<ThreadBackendTask> logger)
        {
            Logger = logger;
        }
        
        public virtual async Task RunAsync()
        {
            try
            {
                var configs = CreateConfigs(ItemsCount);
                var results = await ExecuteAsync(configs);
                WriteResults(results);
            }
            catch (Exception e)
            {
                Logger.LogError(e.Message);
            }
        }

        /// <summary>
        /// Creates a collection of thread task item configurations.
        /// </summary>
        /// <param name="count">The number of configurations to create.</param>
        /// <returns>A collection of <see cref="ThreadTaskItemConfig"/>.</returns>
        protected virtual IEnumerable<ThreadTaskItemConfig> CreateConfigs(int count) 
            => Enumerable.Range(0, count).Select(CreateConfig);

        /// <summary>
        /// Creates a thread task item configuration.
        /// </summary>
        /// <param name="number">The number for the configuration.</param>
        /// <returns>A <see cref="ThreadTaskItemConfig"/> instance.</returns>
        protected virtual ThreadTaskItemConfig CreateConfig(int number) => new(number);
   
        /// <summary>
        /// Executes the given configurations concurrently and returns the results.
        /// </summary>
        /// <param name="configs">The configurations to execute.</param>
        /// <returns>A collection of <see cref="ThreadTaskItemResult"/>.</returns>
        protected virtual async Task<IEnumerable<ThreadTaskItemResult>> ExecuteAsync(
            IEnumerable<ThreadTaskItemConfig> configs)
        {
            var results = new ConcurrentBag<ThreadTaskItemResult>();

            await Parallel.ForEachAsync(configs, async (config, token) =>
            {
                var result = await ExecuteAsync(config);
                results.Add(result);
            });

            return results;
        }

        /// <summary>
        /// Executes a single configuration asynchronously.
        /// </summary>
        /// <param name="config">The configuration to execute.</param>
        /// <returns>A <see cref="ThreadTaskItemResult"/> instance.</returns>
        protected virtual async Task<ThreadTaskItemResult> ExecuteAsync(ThreadTaskItemConfig config)
        {
            var message = $"Message {config.Number}";
            
            await Task.Delay(100);

            if (config.Number % 10 == 0) // Introduce occasional errors for demonstration
            {
                message = null;
            }

            return new ThreadTaskItemResult(message, config); // Include the config for error handling
        }

        /// <summary>
        /// Writes the results of the executed configurations to the log.
        /// </summary>
        /// <param name="results">The results to write.</param>
        protected virtual void WriteResults(IEnumerable<ThreadTaskItemResult> results)
        {
            foreach (var result in results)
            {
                if (result.Message is null)
                {
                    Logger.LogError($"Item {result.Config.Number} was not processed");
                }
                else
                {
                    Logger.LogInformation($"Successfully processed task: {result.Message}");
                }
            }
        }
    }
}
