using ConsoleApp.Models;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Tasks;

/// <summary>
/// Represents a backend task to process products.
/// </summary>
public class ExpressionBackendTask : IBackendTask
{
    protected readonly ILogger<ExpressionBackendTask> Logger;

    /// <summary>
    /// Initializes a new instance of the <see cref="ExpressionBackendTask"/> class.
    /// </summary>
    /// <param name="logger">The logger instance.</param>
    public ExpressionBackendTask(ILogger<ExpressionBackendTask> logger)
    {
        Logger = logger;
    }
    
    public async Task RunAsync()
    {
        var fields = new[]
            { nameof(Product.Id), nameof(Product.Name), nameof(Product.Description), nameof(Product.Price) };

        Logger.LogInformation($"Got filter fields: {string.Join(", ", fields)}");

        var products = await LoadRecords<Product>(fields);

        if (products is null || !products.Any())
        {
            Logger.LogInformation("Products were not loaded");
            return;
        }
        
        Logger.LogInformation($"Loaded {products.Count()} products");

        WriteRecords(products, fields);
    }

    /// <summary>
    /// Loads records of the specified type with selected fields asynchronously.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <param name="fields">The fields to be loaded.</param>
    /// <returns>An <see cref="IQueryable{T}"/> of loaded records.</returns>
    protected virtual async Task<IQueryable<T>> LoadRecords<T>(params string[] fields) where T : IPrimaryEntity
    {
        var records = LoadRecords<T>();

        await Task.Delay(100);

        return records;
    }

    /// <summary>
    /// Writes a record with the specified fields to the log.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <param name="record">The record to be written.</param>
    /// <param name="fields">The fields to be written.</param>
    protected virtual void WriteRecord<T>(T record, params string[] fields) where T : IPrimaryEntity
    {
        Logger.LogInformation($"Record {record.Id}:");

        var fieldMessages = fields.Select(field => $"{field} = record[field]");

        Logger.LogInformation(string.Join("; ", fieldMessages));
    }

    /// <summary>
    /// Writes multiple records with the specified fields to the log.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <param name="records">The records to be written.</param>
    /// <param name="fields">The fields to be written.</param>
    private void WriteRecords<T>(IEnumerable<T> records, string[] fields) where T : IPrimaryEntity
    {
        foreach (var record in records) WriteRecord(record, fields);
    }

    /// <summary>
    /// Loads records of the specified type.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <returns>An <see cref="IQueryable{T}"/> of loaded records.</returns>
    protected IQueryable<T> LoadRecords<T>() where T : IPrimaryEntity
    {
        return new[]
        {
            new Product(1, "product 1", "description 1", 50.5m),
            new Product(2, "product 2", "description 2", 25.3m),
            new Product(3, "product 3", "description 3", 165.1m)
        }.Cast<T>().AsQueryable();
    }
}