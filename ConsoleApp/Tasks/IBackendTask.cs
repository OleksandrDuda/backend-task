namespace ConsoleApp.Tasks;

public interface IBackendTask {
  
  /// <summary>
  /// Executes the backend task asynchronously.
  /// </summary>
  Task RunAsync();
}
