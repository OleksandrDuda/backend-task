﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Tasks.BackendTaskLimited;

/// <summary>
/// Represents a backend task that processes multiple items concurrently using limited amount of threads.
/// </summary>
public class ThreadBackendTaskLimited : ThreadBackendTask
{
    private readonly SemaphoreSlim _semaphore = new(MaxConcurrentTasks);
    private const int MaxConcurrentTasks = 5;

    public ThreadBackendTaskLimited(ILogger<ThreadBackendTaskLimited> logger) : base(logger) { }

    protected override async Task<IEnumerable<ThreadTaskItemResult>> ExecuteAsync(
        IEnumerable<ThreadTaskItemConfig> configs)
    {
        var results = new ConcurrentBag<ThreadTaskItemResult>();
        var tasks = configs.Select(async config =>
        {
            await _semaphore.WaitAsync();
            try
            {
                var result = await ExecuteAsync(config);
                results.Add(result);
            }
            catch (Exception e)
            {
                Logger.LogError($"Task '{config.Number}' was not proceeded. Error: {e.Message}");
            }
            finally
            {
                _semaphore.Release();
            }
        });

        await Task.WhenAll(tasks);

        return results;
    }
}