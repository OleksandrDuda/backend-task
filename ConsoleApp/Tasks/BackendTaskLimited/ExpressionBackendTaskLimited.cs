﻿using System.Linq.Expressions;
using ConsoleApp.Extensions;
using ConsoleApp.Models;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Tasks.BackendTaskLimited;

/// <summary>
/// Represents a backend task to process products with limited fields.
/// </summary>
public class ExpressionBackendTaskLimited : ExpressionBackendTask
{
    public ExpressionBackendTaskLimited(ILogger<ExpressionBackendTaskLimited> logger) : base(logger) { }
    
    protected override async Task<IQueryable<T>> LoadRecords<T>(params string[] fields)
    {
        try
        {
            var records = LoadRecords<T>();
            
            var fieldsSelector = CreateSelector<T>(fields);
            
            var resultQuery = records.Select(fieldsSelector);
            
            await Task.Delay(100);

            return resultQuery;
        }
        catch (Exception e)
        {
            Logger.LogError($"Load records was not proceeded. Error: {e.Message}");
        }

        return null;
    }

    protected override void WriteRecord<T>(T record, params string[] fields)
    {
        try
        {
            var fieldMessages = fields
                .Select(field => $"{field} = {record.GetPropertyValue(field)}")
                .Aggregate((i, j) => i + "; " + j);
            
            Logger.LogInformation($"Record {record.Id}:\n {fieldMessages}");
        }
        catch (Exception e)
        {
            Logger.LogError($"Write records was not proceeded. Error: {e.Message}");
        }
    }
    
    private Expression<Func<T,T>> CreateSelector<T>(string[] fields)
    {
        var parameter = Expression.Parameter(typeof(T), "Limited" + nameof(Product));
        var bindings =
            fields.Select(field =>
                Expression.Bind(
                    typeof(T).GetProperty(field) ?? throw new ArgumentException(
                        $"The type '{typeof(T).Name}' does not contain a field named '{field}'."
                    ),
                    Expression.Property(parameter, field)));

        var selector = Expression.Lambda<Func<T, T>>(
            Expression.MemberInit(Expression.New(typeof(T)), bindings),
            parameter);
        
        return selector;
    }
    
    private T CreateMappedObject<T>(T source, string[] fields)
    {
        var targetType = typeof(T);
        var result = Activator.CreateInstance(targetType);

        foreach (var field in fields)
        {
            var sourceProperty = targetType.GetProperty(field);
            if (sourceProperty is not null)
            {
                var value = sourceProperty.GetValue(source);
                targetType.GetProperty(field)?.SetValue(result, value);
            }
            else
            {
                throw new ArgumentException($"The type '{typeof(T).Name}' does not contain a field named '{field}'.");
            }
        }

        return (T)result;
    }

}