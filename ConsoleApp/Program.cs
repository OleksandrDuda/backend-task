﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using ConsoleApp.Tasks;
using ConsoleApp.Tasks.BackendTaskLimited;

var serviceCollection = new ServiceCollection();
ConfigureServices(serviceCollection);

var serviceProvider = serviceCollection.BuildServiceProvider();

//await RunBackendTasks(serviceProvider);
await RunLimitedBackendTasks(serviceProvider);

return;

void ConfigureServices(IServiceCollection services)
{
    services.AddLogging(configure => configure.AddConsole());
    
    services.AddTransient<ThreadBackendTask>();
    services.AddTransient<ThreadBackendTaskLimited>();
    
    services.AddTransient<ExpressionBackendTask>();
    services.AddTransient<ExpressionBackendTaskLimited>();
}

async Task RunBackendTasks(IServiceProvider servicesProvider)
{
    var threadBackendTask = servicesProvider.GetService<ThreadBackendTask>();
    await threadBackendTask.RunAsync();

    var expressionBackendTask = servicesProvider.GetService<ExpressionBackendTask>();
    await expressionBackendTask.RunAsync();
}

async Task RunLimitedBackendTasks(IServiceProvider servicesProvider)
{
    var threadBackendTask = servicesProvider.GetService<ThreadBackendTaskLimited>();
    await threadBackendTask.RunAsync();

    var expressionBackendTask = servicesProvider.GetService<ExpressionBackendTaskLimited>();
    await expressionBackendTask.RunAsync();
}