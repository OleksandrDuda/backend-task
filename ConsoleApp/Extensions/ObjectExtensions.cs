﻿using System.Reflection;

namespace ConsoleApp.Extensions;

public static class ObjectExtensions
{
    /// <summary>
    /// Searches for the specified <i>public</i> property and returns its value
    /// </summary>
    /// <param name="obj">Object to retrieve property</param>
    /// <param name="propertyName">Name of property</param>
    /// <returns>The property value of the specified object</returns>
    /// <exception cref="ArgumentException">Object was null</exception>
    /// <exception cref="ArgumentNullException">Property was not found on given object</exception>
    public static object GetPropertyValue(this object obj, string propertyName)
    {
        ArgumentNullException.ThrowIfNull(obj);

        var propertyInfo = obj.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
        
        if (propertyInfo is null)
            throw new ArgumentException($"Property '{propertyName}' not found on object of type {obj.GetType().Name}.");

        return propertyInfo.GetValue(obj);
    }
}