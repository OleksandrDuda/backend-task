# Overview!

Thank you for your tasks, that was interesting :)

# Changes Made
### Added Dependency Injection:
```csharp
    services.AddTransient<ThreadBackendTask>();
    services.AddTransient<ExpressionBackendTask>();
```

### Added Logger:
- Replaced `Console.WriteLine` statements with ILogger and configured logger to write into console.
```csharp
    services.AddLogging(configure => configure.AddConsole());;
```
- Log example
```bash
    info: ConsoleApp.Tasks.BackendTaskLimited.ThreadBackendTaskLimited[0]
    Successfully processed task: Message 77
    fail: ConsoleApp.Tasks.BackendTaskLimited.ThreadBackendTaskLimited[0]
    Item 70 was not processed
```

### ThreadBackendTask
- Added new class `ThreadBackendTaskLimited` inherited from `ThreadBackendTask`.
- Overrode `ExecuteAsync` method to limit number of threads using `Semaphore`.
- Made changes in `ThreadBackendTask` to use `Parallel.ForEach` instead of `Task.WhenAll`.

### ExpressionBackendTask
- Added new class `ExpressionBackendTaskLimited` inherited from `ExpressionBackendTask`.
- Created new extension method `GetPropertyValue` to return values from property by name.
- Overrode `WriteRecord` method to use `GetPropertyValue` and return real values of fields.

### Additional
- Some code refactoring done
- Added documentation for classes and methods